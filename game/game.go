package main

import "fmt"

func main() {
	var l1 Location
	fmt.Println("l1", l1)
	l1.X = 100 // no getters/setters
	fmt.Println("l1.X", l1.X)

	l2 := Location{10, 20}
	fmt.Println("l2", l2)
	fmt.Printf("l2  (v): %v\n", l2)
	fmt.Printf("l2 (+v): %+v\n", l2)
	fmt.Printf("l2 (#v): %#v\n", l2) // use this in debug/log

	a, b := 1, "1"
	fmt.Printf("a=%v, b=%v\n", a, b)
	fmt.Printf("a=%#v, b=%#v\n", a, b)

	// prefer this method, it's future proof
	l3 := Location{Y: 30} //, X: 40}
	fmt.Printf("l3: %#v\n", l3)

	fmt.Println(NewLocation(10, 20))
	fmt.Println(NewLocation(10, 2000))

	l3.Move(100, 200)
	// Location.Move(l3, 100, 200)
	// l3.Move: bound, Location.Move: unbound
	fmt.Printf("l3 (move): %#v\n", l3)

	p1 := &Player{
		Name:     "Parzival",
		Location: Location{10, 20},
	}
	fmt.Printf("p1: %#v\n", p1)
	fmt.Printf("p1.X: %#v\n", p1.X)
	fmt.Printf("p1.Location.X: %#v\n", p1.Location.X)
	p1.Move(50, 7)
	fmt.Printf("p1 (move): %#v\n", p1)

	// fmt.Println(p1.Found("Gold")) // error
	fmt.Println(p1.Found(Key(7))) // error
	fmt.Println(p1.Found(Copper)) // nil
	fmt.Println(p1.Found(Copper)) // nil
	fmt.Println(p1.Keys)          // ["Copper"]

	ms := []Mover{
		&l1,
		p1,
	}
	moveAll(ms, 400, 500)
	for _, m := range ms {
		fmt.Println(m)
	}

	fmt.Println("Crystal", Crystal)
}

/* stringer tool

$ go install golang.org/x/tools/cmd/stringer@latest
$ export PATH="$(go env GOPATH)/bin:${PATH}"
$ strings -type Key
$ cat key_string.go
*/

// implement fmt.Stringer
func (k Key) String() string {
	switch k {
	case Crystal:
		return "crystal"
	case Jade:
		return "jade"
	case Copper:
		return "copper"
	}

	return fmt.Sprintf("<Key %d>", k)
}

/* thought experiment
Go:
type Reader interface {
	Read(p []byte) (int, error)
}

Python:
type Reader interface {
	Read(n int) (p []byte, error)
}

type Sortable interface {
	Len() int
	Less(i, j int) bool
	Swap(i, j int)
}
*/

// Interfaces say what we need, not what we provide
// Interface are small (2-3 methods)
// Rule of thumb: accept interface, return types

type Mover interface {
	Move(int, int)
}

// Mover can be either a Player or Location
func moveAll(ms []Mover, x, y int) {
	for _, m := range ms {
		m.Move(x, y)
	}
}

/*
	Exercise:

- Add a Keys field to the player which is a slice of strings
- Define 3 known keys: Copper, Jade, Crystal
- Add a "Found(k Key) error"
	- Err if k is not one of the known keys
	- Add a key only once

	p1.Found("Gold") // error
	p1.Found(Copper) // nil
	p1.Found(Copper) // nil
	fmt.Println(p1.Keys) // ["Copper"]
*/

func (p *Player) Found(key Key) error {
	if !(key == Copper || key == Jade || key == Crystal) {
		return fmt.Errorf("unknown key: %#v", key)
	}

	if !p.hasKey(key) {
		p.Keys = append(p.Keys, key)
	}
	return nil
}

func (p *Player) hasKey(key Key) bool {
	for _, k := range p.Keys {
		if k == key {
			return true
		}
	}
	return false
}

/*
const (

	Copper  = "copper"
	Jade    = "jade"
	Crystal = "crystal"

)
*/
type Key byte

const (
	Copper Key = 1 + iota
	Jade
	Crystal
)

type Player struct {
	Name string
	Keys []Key
	//	X        float64
	Location // Player embeds Location
}

// l is called "the receiver"
// When mutating, use a pointer receiver

func (l *Location) Move(x, y int) {
	l.X = x
	l.Y = y
}

/*
"new" functions
func NewLocation(x, y int) Location
func NewLocation(x, y int) *Location
func NewLocation(x, y int) (Location, error)
func NewLocation(x, y int) (*Location, error)
*/
func NewLocation(x, y int) (*Location, error) {
	if x < 0 || x > maxX || y < 0 || y > maxY {
		return nil, fmt.Errorf("%d/%d out of range %d/%d", x, y, maxX, maxY)
	}

	loc := Location{X: x, Y: y}
	return &loc, nil // go does escape analysis and will allocate loc on the heap
}

const (
	maxX = 1000
	maxY = 600
)

type Location struct {
	X int
	Y int
}

/* Back story:
We have a 2d game and we want to sort player by distance from a location (x, y)

type Player struct{}

func sortByDistance(ps []Player, x, y int) {

}

*/
