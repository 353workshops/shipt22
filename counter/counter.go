package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

func main() {
	/*
		var mu sync.Mutex
		count := 0
	*/
	var count int64

	var wg sync.WaitGroup

	for n := 0; n < 10; n++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for i := 0; i < 1_000; i++ {
				/*
					mu.Lock()
					count++
					mu.Unlock()
				*/
				atomic.AddInt64(&count, 1)
				time.Sleep(time.Microsecond)
			}
		}()
	}
	wg.Wait()

	fmt.Println(count)
}

// go run -race counter.go
// -race is supported by build & test as well
// common practice: use -race in tests
