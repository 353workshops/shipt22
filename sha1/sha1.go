package main

import (
	"compress/gzip"
	"crypto/sha1"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	fileName := "../_extra/http.log.gz"
	// GoLand:
	// fileName := "_extra/http.log.gz"
	fmt.Println(fileSHA1(fileName))

	fileName = "sha1.go"
	fmt.Println(fileSHA1(fileName))
}

// decompress the file only if it ends with .gz
// hint: strings.HasSuffix

// $ cat _extra/http.log.gz | gunzip | sha1sum
// $ cat sha1.go | sha1sum
func fileSHA1(fileName string) (string, error) {
	// cat _extra/http.log.gz
	file, err := os.Open(fileName)
	if err != nil {
		return "", err
	}
	defer file.Close()

	var r io.Reader = file
	if strings.HasSuffix(fileName, ".gz") {
		// | gunzip
		r, err = gzip.NewReader(file)
		if err != nil {
			return "", err
		}
	}

	// | sha1sum
	w := sha1.New()
	if _, err := io.Copy(w, r); err != nil {
		return "", err
	}

	sig := w.Sum(nil)
	return fmt.Sprintf("%x", sig), nil
}
