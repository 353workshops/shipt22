package main

import "fmt"

func main() {
	// Use any is a "code smell"
	var i any // interface{} go < 1.18

	i = 7
	fmt.Println(i)
	i = "hi"
	fmt.Println(i)

	s := i.(string) // type assertion
	fmt.Println(s)

	// n := i.(int) // will panic
	n, ok := i.(int) // "comma, OK"
	fmt.Println(n, ok)

	switch i.(type) { // type switch
	case int:
		fmt.Println("int")
	case float64:
		fmt.Println("float64")
	default:
		fmt.Printf("unknown type: %T\n", i)
	}
}
