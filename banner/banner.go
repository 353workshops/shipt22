package main

import (
	"fmt"
	"unicode/utf8"
)

func main() {
	banner("Go", 6)
	banner("G☺", 6)

	/*
		fmt.Println("Go", len("Go"))
		fmt.Println("Go", len("G☺"))
	*/

	s := "Hello"
	// s[0] = 'h'
	fmt.Println(s)
	fmt.Printf("s = %-20s!\n", s)

	// indices
	for c := range s {
		fmt.Printf("%v\n", c)
	}

	// indices + values
	for i, c := range s {
		fmt.Printf("%d -> %c\n", i, c)
	}

	s1 := s[:2] // slicing, half-closed range. You get first index up to but not including last
	fmt.Println("s1", s1)
	// slicing works on bytes (not characters/runes)
}

func banner(text string, width int) {
	// len returns size of bytes (vs characters)
	// offset := (width - len(text)) / 2
	offset := (width - utf8.RuneCountInString(text)) / 2

	for i := 0; i < offset; i++ {
		fmt.Print(" ")
	}
	fmt.Println(text)
	for i := 0; i < width; i++ {
		fmt.Print("-")
	}
	fmt.Println()
}

// "Go", 6
//   Go
// ------
