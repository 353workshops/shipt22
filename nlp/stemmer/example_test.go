package stemmer_test

import (
	"fmt"

	"github.com/shipt/nlp/stemmer"
)

func ExampleStem() {
	for _, word := range []string{"works", "working", "worked"} {
		stem := stemmer.Stem(word)
		fmt.Println(word, "->", stem)
	}

	// Output:
	// works -> work
	// working -> work
	// worked -> work
}
