package main

import (
	"encoding/json"
	"expvar"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/go-chi/chi/v5"
	"go.uber.org/zap"

	"github.com/shipt/nlp"
	"github.com/shipt/nlp/stemmer"
)

var (
	stemCount = expvar.NewInt("stem.calls")
)

func main() {
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatalf("error: can't create logger - %s", err)
	}

	s := Server{
		log: logger.Sugar(),
	}

	expvar.NewString("version").Set("0.1.2")

	r := chi.NewRouter()
	r.Get("/health", s.healthHandler)
	r.Post("/tokenize", s.tokenizeHandler)
	r.Get("/stem/{word}", s.stemHandler)
	r.Handle("/debug/vars", expvar.Handler())
	/*
		http.HandleFunc("/health", healthHandler)
		http.HandleFunc("/tokenize", tokeizeHandler)
	*/
	/* stdlib routing
	- ends with / : prefix match
	- otherwise: exact match
	*/

	// Option 1 for using "r"
	// http.Handle("/", r)

	// if err := http.ListenAndServe(":8080", nil); err != nil {
	// Option 2 for using r
	addr := os.Getenv("NLPD_ADDR")
	if addr == "" {
		addr = ":8080"
	}
	if err := validateAddress(addr); err != nil {
		log.Fatalf("error: %q: bad address - %s", addr, err)
	}

	s.log.Infow("server starting", "address", addr)
	if err := http.ListenAndServe(addr, r); err != nil {
		log.Fatalf("error: %s", err)
	}
}

func validateAddress(addr string) error {
	i := strings.Index(addr, ":")
	if i == -1 {
		return fmt.Errorf("missing ':'")
	}

	port, err := strconv.Atoi(addr[i+1:])
	if err != nil {
		return err
	}

	if port < 0 || port > 65535 {
		return fmt.Errorf("%d - bad port", port)
	}

	return nil
}

// configuration: defaults < config file < environment < command line

type Server struct {
	log *zap.SugaredLogger
}

// $ curl http://localhost:8080/stem/works
// work

func (s *Server) stemHandler(w http.ResponseWriter, r *http.Request) {
	stemCount.Add(1)
	word := chi.URLParam(r, "word")
	stem := stemmer.Stem(word)
	fmt.Fprintln(w, stem)
}

/*
	Exercise:

Write a /tokenize handler that will read text from request body and return JSON of tokens

$ curl -d"Who's on first?" http://localhost:8080/tokenize
{"tokens": ["who", "on", "first"]}
*/
func (s *Server) tokenizeHandler(w http.ResponseWriter, r *http.Request) {
	/*  Before chi
	if r.Method != http.MethodPost {
		http.Error(w, "bad method", http.StatusMethodNotAllowed)
		return
	}
	*/

	// Step 1: Read, parse & validate
	data, err := io.ReadAll(r.Body)
	if err != nil {
		s.log.Errorw("can't read", "error", err)
		http.Error(w, "can't read", http.StatusBadRequest)
		return
	}
	text := string(data)

	// Step 2: Work
	tokens := nlp.Tokenize(text)

	// Step 3: Marshal & send data
	resp := map[string]any{
		"tokens": tokens,
	}

	data, err = json.Marshal(resp)
	if err != nil {
		http.Error(w, "can't encode to JSON", http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(data)

	/*
		if err := json.NewEncoder(w).Encode(resp); err != nil {
			log.Printf("ERROR: can't encode - %s", err)
		}
	*/
}

func (s *Server) healthHandler(w http.ResponseWriter, r *http.Request) {
	// TODO: Real health check
	fmt.Fprintln(w, "OK")
}
