package nlp

import (
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gopkg.in/yaml.v3"
)

func TestTokenize(t *testing.T) {
	text := "What's on second?"
	tokens := Tokenize(text)
	expected := []string{"what", "on", "second"}
	require.Equal(t, expected, tokens)
	// if tokens != expected { // can't do this in go, can compare slice only to nil
	/* Before testify
	if !reflect.DeepEqual(tokens, expected) {
		t.Fatalf("expected: %#v, got: %#v", expected, tokens)
	}
	*/
}

/*
Exercise: Read test cases from testdata/tokenize_cases.yml
instead of in-memory tokenizeCases

Suggestion: Use a utility function to load the tests.
Use gopkg.in/yaml.v3 to parse YAML
*/

/*
var tokenizeCases = []struct {
	text   string
	tokens []string
}{
	{"", nil},
	{"Who's on first?", []string{"who", "s", "on", "first"}},
}
*/

type testCase struct {
	Text   string
	Tokens []string
	Name   string
}

func loadTokenizeCases(t *testing.T) []testCase {
	file, err := os.Open("testdata/tokenize_cases.yml")
	require.NoError(t, err, "open")
	defer file.Close()

	var tc []testCase
	err = yaml.NewDecoder(file).Decode(&tc)
	require.NoError(t, err, "decode")
	return tc
}

func TestTokenizeTable(t *testing.T) {
	// setup: call a function
	// teardown: defer
	for _, tc := range loadTokenizeCases(t) {
		name := tc.Name
		if name == "" {
			name = tc.Text
		}
		t.Run(name, func(t *testing.T) {
			tokens := Tokenize(tc.Text)
			require.Equal(t, tc.Tokens, tokens)
			/* Before testify
			if !reflect.DeepEqual(tc.tokens, tokens) {
				t.Fatalf("expected: %#v, got: %#v", tc.tokens, tokens)
			}
			*/
		})
	}
}

func TestCI(t *testing.T) {
	// Also see build tags
	// In Jenkins check for BUILD_NUMBER
	if os.Getenv("CI") == "" {
		t.Skip("not in CI")
	}

	t.Log("running")
}

func FuzzTokenize(f *testing.F) {
	f.Add("")

	f.Fuzz(func(t *testing.T, text string) {
		tokens := Tokenize(text)

		lText := strings.ToLower(text)
		for _, tok := range tokens {
			require.Contains(t, lText, tok)
		}
	})
}
