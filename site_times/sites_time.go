package main

import (
	"fmt"
	"io"
	"net/http"
	"time"

	"golang.org/x/sync/errgroup"
)

func main() {
	urls := []string{
		"https://www.google.com",
		"https://www.apple.com",
		"https://httpbin.org/status/404",
	}

	if err := siteTimes(urls); err != nil {
		fmt.Println("ERROR:", err)
	}
}

func siteTimes(urls []string) error {
	var g errgroup.Group

	for _, url := range urls {
		url := url
		g.Go(func() error {
			_, err := siteTime(url)
			if err != nil {
				return fmt.Errorf("%q - %w", url, err)
			}
			return nil
		})
	}
	return g.Wait()
}

func siteTime(url string) (time.Duration, error) {
	start := time.Now()

	resp, err := http.Get(url)
	if err != nil {
		return 0, err
	}

	if resp.StatusCode != http.StatusOK {
		return 0, fmt.Errorf("bad status: %d %s", resp.StatusCode, resp.Status)
	}
	_, err = io.Copy(io.Discard, resp.Body)
	if err != nil {
		return 0, err
	}

	return time.Since(start), nil
}
