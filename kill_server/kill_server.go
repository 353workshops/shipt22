package main

import (
	"errors"
	"fmt"
	"log"
	"os"
)

func main() {
	if err := killServer("app.pid"); err != nil {
		for e := err; e != nil; e = errors.Unwrap(e) {
			fmt.Println("\t", e)
		}
		log.Fatalf("error: %s", err)
	}
}

func killServer(pidFile string) error {
	// open file modes:
	// read:   os.Open
	// write:  os.Create (will delete existing)
	// append: os.OpenFile

	// common idiom: acquire resource, check for error, defer release
	file, err := os.Open(pidFile)
	if err != nil {
		return err
	}
	// defer file.Close() // will be executed when killServer returns
	defer func() {
		if err := file.Close(); err != nil {
			log.Printf("WARNING: can't close %q - %s", pidFile, err)
		}
		// return err // can't do this
	}()

	var pid int
	if _, err := fmt.Fscanf(file, "%d", &pid); err != nil {
		// %w "wraps" err
		return fmt.Errorf("%q: bad pid - %w", pidFile, err)
	}

	// simulate kill
	fmt.Printf("killing %d\n", pid)

	if err := os.Remove(pidFile); err != nil {
		log.Printf("WARNING: can't delete %q - %s", pidFile, err)
	}
	return nil
}
