package main

import (
	"fmt"
	"sort"
)

func main() {
	var s1 []int // s1 is slice of ints
	fmt.Println("s1:", s1, "len:", len(s1))

	s1 = []int{10, 20, 30, 40, 50, 60}
	fmt.Println("s1:", s1, "len:", len(s1))

	s2 := s1[1:4]
	fmt.Println("s2:", s2, "len:", len(s2))

	s2[0] = -1
	fmt.Println("s2:", s2)
	fmt.Println("s1:", s1)

	s2 = append(s2, -2)
	fmt.Println("s2:", s2)
	fmt.Println("s1:", s1)

	var s3 []int
	for i := 0; i < 1000; i++ {
		s3 = appendInt(s3, i)
	}
	fmt.Println("len(s3) =", len(s3))

	a, b := []int{1, 2, 3}, []int{4, 5}
	fmt.Println(concat(a, b)) // [1, 2, 3, 4, 5]

	values := []float64{2, 1, 3}
	fmt.Println(median(values)) // 2
	values = []float64{2, 1, 3, 4}
	fmt.Println(median(values)) // 2.5
	fmt.Println(values)

	family := []string{"Gomez", "Morticia", "Wednesday", "Pugsley"}
	for i := range family { // indices
		fmt.Println(i)
	}

	for i, n := range family { // index + value
		fmt.Println(n, "at", i)
	}

	for _, n := range family { // value
		fmt.Println(n)
	}

	// You can only compare (using ==) a slice to nil
	// s1 == s2 // won't compile
}

/*
	median:

sort values
if odd number of values: return middle
else return average (mean) of middles
*/
func median(values []float64) float64 {
	// Copy so sort won't affect values
	vals := make([]float64, len(values))
	copy(vals, values)

	sort.Float64s(vals)
	i := len(vals) / 2
	if len(vals)%2 == 1 {
		return vals[i]
	}

	m := (vals[i-1] + vals[i]) / 2
	return m
}

func concat(s1, s2 []int) []int {
	s := make([]int, len(s1)+len(s2))
	copy(s, s1)
	copy(s[len(s1):], s2)
	return s
}

func appendInt(s []int, val int) []int {
	i := len(s)
	if len(s) < cap(s) { // underlying array has space
		s = s[:len(s)+1]
	} else { // need to allocate a bigger array & copy
		size := 1 + 2*len(s)
		fmt.Println(len(s), "->", size)
		s1 := make([]int, size)
		copy(s1, s)
		s = s1[:len(s)+1]
	}
	s[i] = val
	return s
}
