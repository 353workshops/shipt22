package main

import (
	"context"
	"fmt"
	"net/http"
	"time"
)

func main() {
	ch1, ch2 := make(chan string), make(chan string)

	go func() {
		time.Sleep(10 * time.Millisecond)
		ch1 <- "one"
	}()

	go func() {
		time.Sleep(20 * time.Millisecond)
		ch2 <- "two"
	}()

	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
	defer cancel()

	select {
	case val := <-ch1:
		fmt.Printf("got %q from ch1\n", val)
	case val := <-ch2:
		fmt.Printf("got %q from ch2\n", val)
		/*
			case <-time.After(time.Millisecond):
			case <-after(time.Millisecond):
		*/
	case <-ctx.Done():
		fmt.Println("timeout")
	}

	done := make(chan struct{})
	for i := 0; i < 3; i++ {
		go worker(i, "https://google.com", done)
	}
	time.Sleep(10 * time.Millisecond)
	close(done) // notify all to finish

	// Give goroutines time to print
	time.Sleep(10 * time.Millisecond)
}

func worker(id int, url string, done chan struct{}) int {
	ch := make(chan int)
	go func() {
		resp, err := http.Get(url)
		if err != nil {
			ch <- -1
			return
		}
		ch <- resp.StatusCode
	}()

	select {
	case code := <-ch:
		return code
	case <-done:
		fmt.Printf("%d:%q aborting\n", id, url)
		return -2
	}
}

// what time.After does (ish)
func after(d time.Duration) chan time.Time {
	ch := make(chan time.Time)
	go func() {
		time.Sleep(d)
		ch <- time.Now()
	}()
	return ch
}
