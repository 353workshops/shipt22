package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
)

// What is the most common word (ignoring case) in shrelock.txt
/* Tasks
- Read one line at a time
	bufio.Scanner
- Split to words
	regex
- Find most common
*/

// `hello` is a "raw" string. \ is just a \, multi line

var (
	// "How are you?" -> [how are you]
	wordRe = regexp.MustCompile(`[A-Za-z]+`)
)

func main() {
	// mapDemo()
	// return

	file, err := os.Open("../_extra/sherlock.txt")
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	defer file.Close()

	s := bufio.NewScanner(file)
	freq := make(map[string]int) // word -> count
	for s.Scan() {
		words := wordRe.FindAllString(s.Text(), -1)
		for _, w := range words {
			w = strings.ToLower(w)
			freq[w]++
		}
		/*
			fmt.Printf("text: %#v\n", s.Text())
			fmt.Println(words)
		*/
	}
	if err := s.Err(); err != nil {
		log.Fatalf("error: %s", err)
	}

	maxW, maxC := "", 0
	for w, c := range freq {
		if c > maxC {
			maxW, maxC = w, c
		}
	}
	fmt.Println(maxW)
}

func mapDemo() {
	// symbol -> price in USD
	stocks := map[string]float64{
		"AAPL": 142.60,
		"AMZN": 88.42,
	}
	// var stocks map[string]float64 // un-initialized map (nil)
	// stocks := map[string]int{} // initialized map

	v := stocks["TSLA"]
	fmt.Println(v)
	v, ok := stocks["TSLA"]
	if ok {
		fmt.Println(v)
	} else {
		fmt.Println("TSLA not found")
	}

	for k := range stocks { // keys
		fmt.Println(k)
	}

	for k, v := range stocks { // keys & values
		fmt.Println(k, "->", v)
	}
	stocks["TSLA"] = 180.44 // will panic on un-initialized map
	fmt.Println(stocks)

	delete(stocks, "AAPL")
	fmt.Println(stocks)
	delete(stocks, "AAPL")

}
