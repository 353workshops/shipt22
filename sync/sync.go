package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func main() {
	p := Payment{
		From:   "Wile. E. Coyote",
		To:     "ACME",
		Amount: 123.45,
	}

	p.Pay()
	p.Pay()

	var wg sync.WaitGroup
	const size = 3
	// wg.Add(size)
	for id := 0; id < size; id++ {
		wg.Add(1)
		id := id
		go func() {
			defer wg.Done()
			worker(id)
		}()
	}
	wg.Wait() // block until wg counter reaches 0
}

func (p *Payment) Pay() {
	p.once.Do(func() {
		p.pay(time.Now().UTC())
	})
}

func worker(id int) {
	fmt.Printf("%d started\n", id)
	// simulate work
	n := rand.Intn(100)
	time.Sleep(time.Duration(n) * time.Millisecond)
	fmt.Printf("%d ended\n", id)
}

func (p *Payment) pay(t time.Time) {
	ts := t.Format(time.RFC3339)
	fmt.Printf("[%s] %s -> $%.2f -> %s\n", ts, p.From, p.Amount, p.To)
}

type Payment struct {
	From   string
	To     string
	Amount float64 // USD

	once sync.Once
}
