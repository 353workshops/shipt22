package main

import (
	"fmt"
	"time"
)

func main() {
	go fmt.Println("goroutine")
	fmt.Println("main")

	// Fix 2: Local variable
	for i := 0; i < 5; i++ {
		i := i // shadow's i from line 13
		go func() {
			fmt.Println(i) // i from line 14
		}()
	}

	/* Fix 1: Pass an argument
	for i := 0; i < 5; i++ {
		go func(n int) {
			fmt.Println(n)
		}(i)
	}
	*/

	/*
		for i := 0; i < 5; i++ {
			go func() {
				fmt.Println(i) // BUG: All goroutines use the same i
			}()
		}
	*/

	time.Sleep(100 * time.Millisecond)

	ch := make(chan int)
	go func() {
		ch <- 7 // send
	}()
	val := <-ch // receive
	fmt.Println("val:", val)

	go func() {
		for i := 0; i < 3; i++ {
			ch <- i
		}
		close(ch)
	}()

	for val := range ch {
		fmt.Println("val (for):", val)
	}
	/* the above is a syntactic sugar for
	for {
		val, ok := <- ch
		if !ok {
			break
		}
		fmt.Println("val (for):", val)
	}
	*/

	val = <-ch
	fmt.Println("val (closed):", val)

	val, ok := <-ch
	fmt.Println("val (closed):", val, "ok:", ok)

	values := []int{8, 2, 1, 3, 4, 5, 7, 6}
	fmt.Println(sleepSort(values)) // [1 2 3 4 5 6 7 8]
}

/*
	sleepSort

for every value "n" in values spin a goroutine that will
  - sleep n milliseconds
  - send n over a channel

collect values from the channel to a slice and return it
*/
func sleepSort(values []int) []int {
	ch := make(chan int)
	for _, n := range values {
		n := n
		go func() {
			time.Sleep(time.Duration(n) * time.Millisecond)
			ch <- n
		}()
	}

	var out []int
	// for i := 0; i < len(values); i++ {
	for range values {
		n := <-ch
		out = append(out, n)
	}

	return out
}

/* Goroutine semantics
- send/receive will block until opposite operation (*)
	- buffered channel of cap n has n non-blocking sends
- receive from a closed channel will return the zero value without blocking
- send or close to a closed channel will panic
- send or receive on a nil channel will block forever
*/

func scopeDemo() {
	// closure bug is not specific to goroutines

	var fns []func()
	for i := 0; i < 5; i++ {
		fn := func() {
			fmt.Println(i)
		}
		fns = append(fns, fn)
	}

	for _, fn := range fns {
		fn()
	}
}
