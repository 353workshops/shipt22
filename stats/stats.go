package main

import "fmt"

func main() {
	iVals := []int{3, 1, 2}
	fmt.Println(Min(iVals))

	fVals := []float64{3, 1, 2}
	fmt.Println(Min(fVals))
}

type Comparable interface {
	int | float64 | string
}

// go >= 1.18
func Min[T Comparable](values []T) (T, error) {
	if len(values) == 0 {
		var zero T
		return zero, fmt.Errorf("min of empty slice")
	}

	m := values[0]
	for _, v := range values[1:] {
		if v < m {
			m = v
		}
	}
	return m, nil
}

func MinFloat64s(values []float64) (float64, error) {
	if len(values) == 0 {
		return 0, fmt.Errorf("min of empty slice")
	}

	m := values[0]
	for _, v := range values[1:] {
		if v < m {
			m = v
		}
	}
	return m, nil
}

func MinInts(values []int) (int, error) {
	if len(values) == 0 {
		return 0, fmt.Errorf("min of empty slice")
	}

	m := values[0]
	for _, v := range values[1:] {
		if v < m {
			m = v
		}
	}
	return m, nil
}
