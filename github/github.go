package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"
)

// RPC: Remote Procedure Call
// Transport/medium + encoding/serialization
// REST: HTTP + JSON

// client: marshal arguments + send to server
// server: unmarshal arguments + call function + marshal response + send to client
// client: unmarshal response

/* encoding/json

Types:

JSON <-> Go
------------
null <-> nil
true/false <-> bool
string <-> string
number <-> [float64], float32, int, int8, int16, int32, int64, uint8, ... uint64
array <-> []any, []T
object <-> map[string]any, struct

MIA: time.Time, []byte, ... (also comments)

API:
JSON []byte -> Go: json.Unmarshal
Go -> JSON []byte: json.Marshal
JSON io.Reader -> Go: json.Decoder
Go -> JSON io.Writer: json.Encoder
*/

func main() {
	/*
		file, err := os.Open("response.json")
		if err != nil {
			log.Fatalf("error: %s", err)
		}

		name, repos, err := decode(file)
		if err != nil {
			log.Fatalf("error: %s", err)
		}
		fmt.Println(name, repos)

		fmt.Println("a.com/" + "user")
		baseURL := "a.com"
		url := fmt.Sprintf("%s/%s", baseURL, "user")
		fmt.Println(url)
	*/
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Millisecond)
	defer cancel()

	name, repos, err := userInfo(ctx, "tebeka")
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	fmt.Println(name, repos)
}

var (
	baseURL = "https://api.github.com/users"
)

// userInfo returns name and public repos from github API
func userInfo(ctx context.Context, login string) (string, int, error) {
	// TODO: URL encoding
	url := fmt.Sprintf("%s/%s", baseURL, login)
	// resp, err := http.Get(url)
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return "", 0, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", 0, err
	}

	if resp.StatusCode != http.StatusOK {
		return "", 0, fmt.Errorf("%q: bad status: %s", login, resp.Status)
	}

	return decode(resp.Body)
}

/*
type userResponse struct {
	// JSON parsed fields must be exported (start with capital letter)
	Name string
	// Public_repos int
	NumRepos int `json:"public_repos"`
}
*/

func decode(r io.Reader) (string, int, error) {
	dec := json.NewDecoder(r)
	// var resp userResponse
	var resp struct { // anonymous struct
		// JSON parsed fields must be exported (start with capital letter)
		Name string
		// Public_repos int
		NumRepos int `json:"public_repos"`
	}
	if err := dec.Decode(&resp); err != nil {
		return "", 0, err
	}

	return resp.Name, resp.NumRepos, nil
}

func get() {
	resp, err := http.Get("https://api.github.com/users/tebeka")
	if err != nil {
		/*
			fmt.Println("ERROR:", err)
			return
		*/
		log.Fatalf("error: %s", err) // log.Printf(...) + os.Exit(1)
	}
	if resp.StatusCode != http.StatusOK {
		log.Fatalf("error: bad status - %s", resp.Status)
	}
	// fmt.Println("content-type:", resp.Header.Get("Content-Type"))
	io.Copy(os.Stdout, resp.Body)
}
