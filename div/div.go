package main

import "fmt"

func main() {
	/*
		fmt.Println(div(7, 3))
		fmt.Println(div(7, 0))
	*/
	fmt.Println(safeDiv(7, 3))
	fmt.Println(safeDiv(7, 0))
}

func safeDiv(a, b int) (q int, err error) {
	// q & err are local variables here (just like a & b)
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("%v", e)
		}
	}()

	return div(a, b), nil

	/* I don't like this style of coding
	q = div(a, b)
	return
	*/
}

func div(a, b int) int {
	return a / b
}
